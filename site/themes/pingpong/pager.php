<?php
/**
 * User: Oleg0s
 * Date: 07.07.2015
 * Time: 10:45
 */

//$args = array();
$pages = paginate_links( array(
    'type' => 'array',
    'prev_text'          => '«',
    'next_text'          => '»',
    )
);

if( is_array( $pages ) ) {
    $paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
    echo '<nav><ul class="pagination">';
    foreach ( $pages as $page ) {
        if(strripos($page, 'current')){
            echo "<li class='active'>$page</li>";
        } else {
            echo "<li>$page</li>";
        }
    }
    echo '</ul></nav>';
}

