/**
 * Created by Oleg0s on 12.08.2015.
 */

$(document).ready(function() {
    $( ".category_link" ).each(function( ) {
        var button = '<li><button type="button" class="btn btn-sm btn-link btn-shop" id="'+this.id+'link">'+this.innerText+'</button></li>';
//        var button = '<li><a href="#'+this.id+'" class="btn btn-link btn-shop">'+this.innerText+'</a></li>';
        $("#category_menu").append(button);
    });

    $(".btn-shop").click(function(e) {
        e.preventDefault();
        goToByScroll(this.id);
    });

    $( document.body ).on( "added_to_cart", function( event, fragments, cart_hash ) {
        var bucket_coord = $('.nav-bucket').offset();
        var button_coord = $('a.added').offset();
        $('#bee').css({top: button_coord.top - $(window).scrollTop(), left: button_coord.left - $(window).scrollLeft()})
            .show()
            .animate({top: bucket_coord.top - $(window).scrollTop(), left: bucket_coord.left - $(window).scrollLeft()}, 500, function(){
                $(this).fadeOut();
                updateMinicart();
            });
        $('a.added').removeClass('added');
    });
});

/**
 * submit AJAX request to update mini-cart
 */
function updateMinicart() {
    //console.log(ajax_object.ajax_url);
    var data = {
        'action': 'pp_ajax_minicart_html'
    };
    $.post(ajax_object.ajax_url, data, function (response) {
        if(response) {
            $('#pp_cart_top').html(response);
        }
    });
}

function goToByScroll(id){
    id = id.replace("link", "");
    $('html,body').animate({
            scrollTop: $('#'+id).offset().top-100},
        'slow');
}


