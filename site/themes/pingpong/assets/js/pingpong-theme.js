/**
 * Created by Oleg0s on 02.07.2015.
 */

var timeoutId;

$(document).ready(function(){
        var contentHeight = $(window).height() - $('#main-navbar').height() - $('footer').height()-5;
        $('.theme-showcase').css('min-height', contentHeight);

        correctMenuHeight();

        pp_sisters();

        $(window).resize(function(){
            correctMenuHeight();
        }).scroll(function() {
            correctMenuHeight();
            if ( $(document).scrollTop() > 50 ) {
                $('#scrollup').fadeIn('slow');
            } else {
                $('#scrollup').fadeOut('slow');
            }
        });

        /*Scroll up image*/
        $('#scrollup').mouseover( function(){
            $( this ).animate({opacity: 0.65},100);
        }).mouseout( function(){
            $( this ).animate({opacity: 0.8},100);
        }).click( function(){
            $('html, body').animate({scrollTop:0}, 1000);
            //window.scroll(0 ,0);
            return false;
        });

        var hash = window.location.hash;
        if (hash === '#register'){
            $('ul.nav a[href="#event-register"]').tab('show');
        }

    }
);

function correctMenuHeight() {
    if ($(document).scrollTop() > 50 || $(document).width() < 992) {
        $('#main-navbar').addClass('navbar-shrink').removeClass('navbar-expand').css('top', $('#wpadminbar').height());
        $('.hello').fadeOut("slow");
    } else {
        $('#main-navbar').addClass('navbar-expand').removeClass('navbar-shrink').css('top', $('#wpadminbar').height());
        $('.hello').fadeIn("slow");
    }
}

function pp_sisters(){
    //ninja forms Вид мероприятия
    $('#ninja_forms_field_6').change(function(){
        var value = $(this).val();
        if (value == 'Другое ...'){
            $('#ninja_forms_field_9_div_wrap').fadeIn();
            $('#ninja_forms_field_9').val('').focus();
        } else {
            $('#ninja_forms_field_9_div_wrap').fadeOut();
            $('#ninja_forms_field_9').val(value);
        }
    }).change();
    //ninja forms Место проведения
    $('#ninja_forms_field_11').change(function(){
        var value = $(this).val();
        if (value == 'Другое ...'){
            $('#ninja_forms_field_12_div_wrap').fadeIn();
            $('#ninja_forms_field_12').val('').focus();
        } else {
            $('#ninja_forms_field_12_div_wrap').fadeOut();
            $('#ninja_forms_field_12').val(value);
        }
    }).change();

    $('.products li img').hover(function(){
        $(this).animate({left: "0"}, 500);
    }, function(){
        $(this).animate({left: "30%"}, 500);
    });

    $('#pp_cart_top').hover(function(){
        clearTimeout(timeoutId);
        var bucket_coord = $(this).offset();
        $('#pp_minicart').css({top: bucket_coord.top - $(window).scrollTop()+$(this).height()+2, left: bucket_coord.left - $(window).scrollLeft()}).fadeIn();
    }, function(){
        timeoutId = setTimeout(function() { $('#pp_minicart').fadeOut(); }, 1000);
    });

    $('#pp_minicart').hover(function(){
        clearTimeout(timeoutId);
    }, function(){
        $(this).fadeOut();
    });
}

