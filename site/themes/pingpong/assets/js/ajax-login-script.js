/**
 * Created by Oleg0s on 05.07.2015.
 */

jQuery(document).ready(function($) {
    // Perform AJAX login on form submit
    $('form#login').on('submit', function(e){
        $('form#login p.status').addClass('alert alert-info').show().text(ajax_login_object.loadingmessage);
        $.ajax({
            type: 'POST',
            url: ajax_login_object.ajaxurl,
            data: {
                'action': 'pp_ajax_login',
                'username': $('form#login #username').val(),
                'password': $('form#login #password').val(),
                'security': $('form#login #security').val() },
            success: function(data){
                var result = jQuery.parseJSON(data);
                if (result.loggedin == true) {
                    $('form#login p.status').addClass('alert alert-success').text(result.message);
                    document.location.href = ajax_login_object.redirecturl;
                } else {
                    $('form#login p.status').addClass('alert alert-danger').text(result.message);
                }
            }
        });
        e.preventDefault();
    });
});
