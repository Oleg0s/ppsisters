<?php
/**
 * User: Oleg0s
 * Date: 05.07.2015
 * Time: 17:14
 */
?>

<?php get_template_part('templates/modal', 'login'); ?>

<footer>
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 footer-col">
                    <p>Подпишитесь на нас, чтобы быть в курсе событий PP SISTERS:</p>
                </div>
                <div class="col-sm-3 footer-col">
                    <p>Адрес нашей базы:</p>
                </div>
                <div class="col-sm-3 footer-col">
                    <p>Связь с нами:</p>
                </div>
            </div>
        </div>
        <div class="row row-hr"><hr></div>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 footer-col">
                    <form>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div>
                                <input type="text" class="form-control" id="email" placeholder="e-mail">
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-primary">Да</button>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-sm-3 footer-col">
                    <?php
                        $pod = pods( 'ppsisters' );
                        $address = nl2br( $pod -> field( 'pp_our_address' ) );
                        $phone = str_replace(' ', '&nbsp;', $pod -> field( 'pp_sisters_phone'));
                    ?>
                    <p><?php echo $address?></p>
                    <h4 class="phone"><span class="glyphicon glyphicon-earphone"></span><?php echo $phone ?></h4>
                </div>
                <div class="col-sm-3 footer-col">
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
            © ping pong sisters. 2014-2015. копирование материалов запрещено
    </div>
</footer>

<div id="scrollup"><span class="glyphicon glyphicon-chevron-up"></span><p>ВВЕРХ</p></div>

<div id="bee" style="position: fixed; display: none; top: 0; left: 0; z-index: 5999"><span class="badge"><span class="glyphicon glyphicon-shopping-cart"></span></span></div>

<div id="pp_minicart" style="display: none; position: fixed; top: 0; left: 300px">
    <?php the_widget( 'WC_Widget_Cart', 'title=' ); ?>
</div>

<?php wp_footer(); ?>

</body>
</html>
