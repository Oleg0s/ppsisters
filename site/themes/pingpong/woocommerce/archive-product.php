<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$root_category = false;
if ( !is_search() && !is_filtered()) {
    $term 			= get_queried_object();
    $parent_id 		= empty( $term->term_id ) ? 0 : $term->term_id;
    $root_category = ($parent_id === 0);
}

get_header( 'shop' ); ?>

<div class="container theme-showcase content-area" id="primary">
    <div class="row">
        <div class="col-sm-9">
            <?php
            /**
             * woocommerce_before_main_content hook
             *
             * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
             * @hooked woocommerce_breadcrumb - 20
             */
            //do_action( 'woocommerce_before_main_content' );
              if (function_exists('pp_woocommerce_breadcrumb')) pp_woocommerce_breadcrumb();
            ?>

            <?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

                <!-- <h1 class="page-title"><?php woocommerce_page_title(); ?></h1> -->

            <?php endif; ?>

            <?php do_action( 'woocommerce_archive_description' ); ?>
            <?php if ( have_posts() ) : ?>
                <?php do_action( 'woocommerce_before_shop_loop' ); ?>

                <?php woocommerce_product_loop_start(); ?>
                <?php if(!$root_category) : ?>
                    <?php woocommerce_product_subcategories(); ?>
                    <?php while ( have_posts() ) : the_post(); ?>
                        <?php wc_get_template_part( 'content', 'product' ); ?>
                    <?php endwhile; // end of the loop. ?>
                <?php else : ?>
                    <?php
                    $result = array();
                    global $post;
                    while ( have_posts() ) : the_post();
                        $product_cats = wp_get_post_terms( get_the_ID(), 'product_cat' );
                        ob_start();
                        wc_get_template_part( 'content', 'product' );
                        $product_html = ob_get_clean();
                        foreach($product_cats as $cat){
                            $result[$cat->term_id] .= $product_html;
                        }
                    endwhile; // end of the loop.
                    foreach($result as $cat_id => $prod) : ?>
                        <div>
                            <div>
                                <?php $term = get_term_by( 'id', $cat_id, 'product_cat', 'ARRAY_A' );?>
                                <h3 class="category_link" id="category_<?php echo $term['slug'] ?>"><?php echo $term['name'] ?></h3>
                            </div>
                            <div>
                                <?php echo $prod ?>
                            </div>
                        </div>
                    <?php endforeach ?>
                <?php endif ?>
                <?php woocommerce_product_loop_end(); ?>

                <?php do_action( 'woocommerce_after_shop_loop' ); ?>
            <?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>
                <?php wc_get_template( 'loop/no-products-found.php' ); ?>
            <?php endif; ?>
            <?php
            /**
             * woocommerce_after_main_content hook
             *
             * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
             */
              //do_action( 'woocommerce_after_main_content' );
            ?>
        </div>
        <!-- Боковая панель -->
        <div class="col-sm-3">
            <?php do_action( 'woocommerce_sidebar' ); ?>
        </div>
    </div>
</div>
<!-- -->


<?php get_footer( 'shop' ); ?>
