<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>


<div class="container theme-showcase content-area" id="primary">
    <main id="main" class="site-main" role="main">
        <div class="row">
            <div class="col-sm-9">
                <?php if (function_exists('pp_woocommerce_breadcrumb')) pp_woocommerce_breadcrumb(); ?>
                <?php while ( have_posts() ) : the_post(); ?>

                    <?php wc_get_template_part( 'content', 'single-product' ); ?>

                <?php endwhile; // end of the loop. ?>
            </div>
            <!-- Боковая панель -->
            <div class="col-sm-3">
                <?php get_template_part( 'sidebar' ); ?>
            </div>
        </div>

    </main><!-- #main -->
</div><!-- container -->

<?php get_footer( 'shop' ); ?>
