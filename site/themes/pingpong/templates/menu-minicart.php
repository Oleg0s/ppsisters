<?php
/**
 * User: Oleg0s
 * Date: 12.08.2015
 * Time: 12:40
 */
?>
<a href="<?php echo site_url('/cart/'); ?>">
    <div class="nav-bucket"><img src=<?php echo get_template_directory_uri() . "/assets/img/bucket.png"?>>&nbsp;<span class="badge"><?php echo WC()->cart->cart_contents_count ?></span></div>
    <?php if (WC()->cart->subtotal>0):?>
        <div class="hello nav-bucket-sum hide-on-small"><?php echo WC()->cart->get_cart_subtotal(); ?></div>
    <?php endif ?>
</a>
