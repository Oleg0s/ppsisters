<?php
/**
 * User: Oleg0s
 * Date: 05.07.2015
 * Time: 21:12
 */

if ( !is_user_logged_in() ) { 
    return '';
}
$avatar = wsl_get_user_custom_avatar( get_current_user_id() );
if(empty($avatar)){
    return '';
}
?>

<div class="nav-filler pull-right">
    <?php echo "<img src='$avatar' class='hello-avatar img-circle'>"; ?>
</div>
