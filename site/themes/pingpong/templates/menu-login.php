<?php
/**
 * User: Oleg0s
 * Date: 05.07.2015
 * Time: 20:35
 */

?>

<?php if ( is_user_logged_in() ) :
    global $current_user;
    get_currentuserinfo();
    ?>
    <div class="hello hide-on-small">
        Привет!<br><?php echo $current_user->display_name?>
    </div>
    <a href="<?php echo site_url('/my-account/'); ?>"><span class="hide-on-small">Профиль&nbsp;</span><span class="glyphicon glyphicon-user" aria-hidden="true"></span></a>
    &nbsp;&nbsp;|&nbsp;&nbsp;
    <a href="<?php echo wp_logout_url(site_url()); ?>"><span class="hide-on-small">Выход&nbsp;</span><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span></a>
<?php else : ?>
    <a href="#" data-toggle="modal" data-target="#loginModal"><span class="hide-on-small">Вход </span><span class="glyphicon glyphicon-log-in" aria-hidden="true"></span></a>
    &nbsp;&nbsp;|&nbsp;&nbsp;
    <a href="#" data-toggle="modal" data-target="#registrationModal"><span class="hide-on-small">Регистрация </span><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
<?php endif ?>