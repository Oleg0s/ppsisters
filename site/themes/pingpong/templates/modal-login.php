<?php
/**
 * User: Oleg0s
 * Date: 05.07.2015
 * Time: 20:43
 */

if ( is_user_logged_in() ) {
    return '';
}

?>

<!-- Login Modal -->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form  id="login" action="login" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="loginModalLabel">ВХОД НА САЙТ</h4>
                </div>
                <div class="modal-body">
                    <p class="status" role="alert"></p>
                    <div class="div-text-centered">
                        <?php do_action( 'wordpress_social_login' ); ?>
                    </div>
                    <div class="html-divider">
                        <div class="line"></div>
                        <span class="badge">или</span>
                    </div>
                    <div class="form-group">
                        <label for="loginEmail">Логин или e-mail</label>
                        <input type="text" class="form-control" id="username" name="username" placeholder="Логин или e-mail">
                    </div>
                    <div class="form-group">
                        <label for="loginPassword">Пароль</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Пароль">
                    </div>
                </div>
                <div class="modal-footer">
                    <a class="lost btn btn-link pull-left" href="<?php echo wp_lostpassword_url(); ?>">Забыли пароль?</a>
                    <button type="submit" class="btn btn-danger big-font" name="submit">Вход&nbsp;<span class="glyphicon glyphicon-log-in" aria-hidden="true"></span></button>
                    <?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Registration Modal -->
<div class="modal fade" id="registrationModal" tabindex="-1" role="dialog" aria-labelledby="registrationModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="registrationModalLabel">РЕГИСТРАЦИЯ</h4>
                </div>
                <div class="modal-body">
                    <div class="div-text-centered">
                        <?php do_action( 'wordpress_social_login' ); ?>
                    </div>
                    <div class="html-divider">
                        <div class="line"></div>
                        <span class="badge">или</span>
                    </div>
                    <div class="form-group">
                        <label for="loginEmail">Ваш e-mail</label>
                        <input type="email" class="form-control" id="registrationEmail" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="loginPassword">Пароль</label>
                        <input type="password" class="form-control" id="registrationPassword1" placeholder="Пароль">
                    </div>
                    <div class="form-group">
                        <label for="loginPassword">Повторите пароль</label>
                        <input type="password" class="form-control" id="registrationPassword2" placeholder="Пароль">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Регистрация</button>
                </div>
            </form>
        </div>
    </div>
</div>
