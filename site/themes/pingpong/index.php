<?php

if (is_front_page() ){
    if(strpos(wp_get_referer(), site_url()) === FALSE){
        get_template_part( 'video', 'intro' );
        return;
    }
}

get_header();
    if (is_front_page() ){
        ?>
        <div class="container theme-showcase content-area" id="primary">
            <main id="main" class="site-main" role="main">
            <?php
            echo 'Mosaic<br>';
            echo do_shortcode("[iconosquare_widget]");
            echo '<br>';
            echo do_shortcode("[ISW id=305]");
            ?>
            </main><!-- #main -->
        </div><!-- container -->
        <?php
    }
    else
    {
?>
    <div class="container theme-showcase content-area" id="primary">
		<main id="main" class="site-main" role="main">
            <div class="row">
                <div class="col-sm-9">
                    <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
                    <?php if ( have_posts() ) : ?>
                        <?php get_template_part( 'loop' , get_post_type() ); ?>
                    <?php else : ?>
                        <?php get_template_part( 'content', 'none' ); ?>
                    <?php endif; ?>
                </div>
                <!-- Боковая панель -->
                <div class="col-sm-3">
                    <?php get_template_part( 'sidebar' ); ?>
                </div>
            </div>

		</main><!-- #main -->
	</div><!-- container -->

<?php } ?>
<?php get_footer(); ?>
