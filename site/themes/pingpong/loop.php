<?php
/**
 * The loop template file.
 *
 * Included on pages like index.php, archive.php and search.php to display a loop of posts
 * Learn more: http://codex.wordpress.org/The_Loop
 *
 * @package storefront
 */

get_template_part( 'pager' );

while ( have_posts() ) : the_post();
     get_template_part( 'content', get_post_type() );
//     get_template_part( 'content', get_post_format() );

    //echo "<div>postformat=" . get_post_type()."</div>";
endwhile;

get_template_part( 'pager' );

