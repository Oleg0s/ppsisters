<?php
/**
 * User: Oleg0s
 * Date: 27.09.2015
 * Time: 12:05
 */

wp_head();
?>

<script type="text/javascript">
    if(window.screen.width<700){
        window.location = "<?php echo home_url() ?>";
    }
</script>


<video autoplay loop poster="<?php echo get_template_directory_uri() ?>/assets/img/Hey_Joe_1.jpg" id="bgvid" muted>
    <source src="<?php echo get_template_directory_uri() ?>/assets/img/Hey_Joe_1.mp4" type="video/mp4">
</video>
<div id="video-intro">
    <div id="intro-logo">
        <a href="<?php echo home_url() ?>"><img src="<?php echo get_template_directory_uri() ?>/assets/img/logo.png"></a>
    </div>
    <?php wp_nav_menu( array('menu' => 'intro') ); ?>
    <div id="intro-about">
        <?php
        $pod = pods( 'ppsisters' );
        $phone = str_replace(' ', '&nbsp;', $pod -> field( 'pp_sisters_phone'));
        $email = get_bloginfo('admin_email');
        ?>
        <h4><span class="glyphicon glyphicon-earphone"></span>&nbsp;<?php echo $phone ?></h4>
        <h4><span class="glyphicon glyphicon-envelope"></span>&nbsp;<?php echo $email ?></h4>

    </div>
</div>
