<?php

if (is_category() || is_archive()){
    ?>
    <div class="page-header">
        <h1>Календарь событий</h1>
        <p>Добро пожаловать на игры, турниры и события от PING PONG SISTERS.</p>
    </div>
    <?php
}

while ( have_posts() ) : the_post();
     get_template_part( 'content', get_post_type() );
endwhile;

get_template_part( 'pager' );

