<?php
/**
 * @package storefront
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> itemscope="" itemtype="http://schema.org/BlogPosting">
    <?php
    if (!is_category() && !is_archive()){
        the_title( '<h1 class="page-header entry-title" itemprop="name headline">','</h1>' );
        echo '<div class="record-content">';
            if ( has_post_thumbnail() ) {
                the_post_thumbnail( 'large', array( 'itemprop' => 'image' ) );
            }
            the_content('читать далее ...');
        echo '</div>';
    } else {
        if (has_post_thumbnail()) {
            echo '<div class="thumbnail-link"><a href="' . get_permalink() . '" title="' . esc_attr(get_the_title($post_id)) . '">';
            the_post_thumbnail('thumbnail', array('itemprop' => 'image', 'class' => 'img-circle'));
            echo '</a></div>';
        }
        the_title(sprintf('<h2 class="entry-title" itemprop="name headline"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h2>');
        the_excerpt();
    }
    ?>
</article><!-- #post-## -->