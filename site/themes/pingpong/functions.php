<?php
/**
 * User: Oleg0s
 * Date: 05.07.2015
 * Time: 17:02
 */

include_once "inc/breadcrumbs.php";

function rzm_woocommerce_support() {
    add_theme_support( 'woocommerce' );
    add_theme_support( 'post-thumbnails' );
}
add_action( 'after_setup_theme', 'rzm_woocommerce_support' );

add_image_size( 'single-header-thumb', 1024, 250, true);

remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );
add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 20 );


function pingpong_wrapper_start() {
    echo '<div class="container theme-showcase" role="main">';
}

function pingpong_wrapper_end() {
    echo '</div> <!-- /container -->';
}
add_action('woocommerce_before_main_content', 'pingpong_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'pingpong_wrapper_end', 10);

function pp_product_title_wrapper_start() {
    echo '<div class="product-title">';
}

function pp_product_title_wrapper_end() {
    echo '</div>';
}
add_action('woocommerce_before_shop_loop_item_title', 'pp_product_title_wrapper_start', 99);
add_action('woocommerce_after_shop_loop_item_title', 'pp_product_title_wrapper_end', 99);


function pp_woocommerce_breadcrumb(){
    $args = array(
        'delimiter'   => '',
        'wrap_before' => '<ol class="breadcrumb" ' . ( is_single() ? 'itemprop="breadcrumb"' : '' ) . '>',
        'wrap_after'  => '</ol>',
        'before'      => '<li>',
        'after'       => '</li>',
        'home'        => _x( 'Home', 'breadcrumb', 'woocommerce' )
    );

    $breadcrumbs = new WC_Breadcrumb();

    if ( $args['home'] ) {
        $breadcrumbs->add_crumb( $args['home'], apply_filters( 'woocommerce_breadcrumb_home_url', home_url() ) );
    }

    $args['breadcrumb'] = $breadcrumbs->generate();

    wc_get_template( 'global/breadcrumb.php', $args );

}
add_action('woocommerce_before_main_content', 'pp_woocommerce_breadcrumb', 5);

function pp_add_scripts(){
    wp_enqueue_style( 'pingpong-style', get_stylesheet_uri(), '', '20150705' );
//    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

    wp_deregister_script('jquery');
    wp_register_script('jquery', "https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js", false, '1.11.2', true);
    wp_enqueue_script('jquery');

    wp_register_script('pp-bootstrap', get_template_directory_uri() . "/assets/js/bootstrap.min.js", array('jquery'), '1.0', true );
    wp_enqueue_script('pp-bootstrap');
    wp_register_script('pp-theme', get_template_directory_uri() . "/assets/js/pingpong-theme.js", array('jquery'), '1.0', true );
    wp_enqueue_script('pp-theme');
    wp_register_script('pp_script', get_template_directory_uri() . "/assets/js/script.js", array('jquery'), '1.0', true );
    wp_enqueue_script('pp_script');
    wp_localize_script( 'pp_script', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' )) );
}
add_action( 'wp_enqueue_scripts', 'pp_add_scripts',	10 );


function pp_ajax_login(){
    // First check the nonce, if it fails the function will break
    check_ajax_referer( 'ajax-login-nonce', 'security' );

    // Nonce is checked, get the POST data and sign user on
    $info = array();
    $info['user_login'] = $_POST['username'];
    $info['user_password'] = $_POST['password'];
    $info['remember'] = true;

    $user_signon = wp_signon( $info, false );
    if ( is_wp_error($user_signon) ){
        echo json_encode(array('loggedin'=>false, 'message'=>'Неверное имя пользователя, e-mail или пароль.'));
    } else {
        echo json_encode(array('loggedin'=>true, 'message'=>'Успешно...'));
    }
    wp_die();
}

function pp_ajax_login_init(){

    wp_register_script('ajax-login-script', get_template_directory_uri() . '/assets/js/ajax-login-script.js', array('jquery'), '1.0', true  );
    wp_enqueue_script('ajax-login-script');

    wp_localize_script( 'ajax-login-script', 'ajax_login_object', array(
        'ajaxurl' => admin_url( 'admin-ajax.php' ),
        'redirecturl' => home_url(),
        'loadingmessage' => 'Пожалуйста, подождите...'
    ));

    add_action( 'wp_ajax_nopriv_pp_ajax_login', 'pp_ajax_login' );
}

if ( !is_user_logged_in() ) {
    add_action( 'init', 'pp_ajax_login_init',	10 );
}


function rzm_disable_saving_subs( $save, $form_id ) {
    $save = false;
    return $save;
}
add_filter( 'ninja_forms_save_submission', 'rzm_disable_saving_subs', 2, 10 );

add_filter( 'excerpt_more', 'pp_excerpt_more' );
function pp_excerpt_more( $more ) {
    return ' ... <a class="read-more btn btn-xs btn-success" href="' . get_permalink( get_the_ID() ) . '">' . __( 'Читать далее', 'pingpong' ) . '</a>';
}

/*Sidebar*/
add_action( 'widgets_init', 'pp_widgets_init' );
function pp_widgets_init() {

    register_sidebar( array(
        'name'          => 'Right sidebar',
        'id'            => 'sidebar-1',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>',
    ) );

}

add_filter( 'woocommerce_product_tabs', 'pp_woo_remove_reviews_tab', 98);
function pp_woo_remove_reviews_tab($tabs) {
    unset($tabs['description'] );
    unset($tabs['reviews']);
    return $tabs;
}

function rzm_text_strings( $translated_text, $text, $domain ) {
    switch ( $translated_text ) {
        case 'Перейти к оплате' :
            $translated_text = 'Перейти к оформлению';
            break;
    }
    return $translated_text;
}
add_filter( 'gettext', 'rzm_text_strings', 20, 3 );

add_shortcode('pp_sisters_phone', 'pp_sisters_phone');
function pp_sisters_phone(){
    $pod = pods( 'ppsisters' );
    $phone =  $pod -> field( 'pp_sisters_phone' );
    $phone = str_replace(' ', '&nbsp;', $phone);
    return '<span class="glyphicon glyphicon-phone"></span>' . $phone ;
}

add_shortcode('pp_sisters_phone_2', 'pp_sisters_phone_2');
function pp_sisters_phone_2(){
    $pod = pods( 'ppsisters' );
    $phone =  $pod -> field( 'pp_sisters_phone_2' );
    $phone = str_replace(' ', '&nbsp;', $phone);
    return '<span class="glyphicon glyphicon-phone"></span>' . $phone ;
}

add_shortcode('pp_sisters_address', 'pp_sisters_address');
function pp_sisters_address(){
    $address = pods( 'ppsisters' ) -> field( 'pp_our_address' );
    $address = nl2br( $address );
    return '<p><span class="glyphicon glyphicon-home"></span> ' . $address . '</p>';
}

add_action( 'wp_ajax_pp_ajax_minicart_html', 'pp_minicart_html' );
add_action( 'wp_ajax_nopriv_pp_ajax_minicart_html', 'pp_minicart_html' );
function pp_minicart_html() {
    get_template_part('templates/menu', 'minicart');
    wp_die();
}
