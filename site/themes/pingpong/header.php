<?php
/**
 * User: Oleg0s
 * Date: 05.07.2015
 * Time: 17:06
 */
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php bloginfo('name'); wp_title( '|' ); ?></title>

    <?php wp_head(); ?>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body role="document">

<!-- Fixed navbar -->
<nav class="navbar navbar-inverse navbar-fixed-top navbar-expand" id="main-navbar">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo site_url(); ?>"><img src=<?php echo get_template_directory_uri() . "/assets/img/logo.png"?> alt="PingPongSisters"></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <?php get_template_part('templates/menu', 'main'); ?>
            </ul>
            <!-- Avatar -->
            <?php get_template_part('templates/menu', 'avatar'); ?>
            <!-- Login -->
            <div class="nav-filler pull-right">
                <div class="nav-user-menu">
                    <?php get_template_part('templates/menu', 'login'); ?>
                </div>
            </div>
            <!-- Bucket -->
            <div class="nav-filler pull-right">
                <div id="pp_cart_top" class="nav-user-menu">
                    <?php get_template_part('templates/menu', 'minicart'); ?>
                </div>
            </div>
        </div><!--/.nav-collapse -->
    </div>
</nav>
