<?php
/**
 * User: Oleg0s
 * Date: 07.08.2015
 * Time: 15:16
 */

if(!function_exists('translate_week_name')) {
    function translate_week_name($text)
    {
        $rgSearch = array(
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday',
            'Sunday'
        );
        $rgReplace = array(
            'Понедельник',
            'Вторник',
            'Среда',
            'Четверг',
            'Пятница',
            'Суббота',
            'Воскресенье'
        );
        return str_replace($rgSearch, $rgReplace, $text);
    }
}

$pod = pods('pp_event', get_the_id());
$event_date = strtotime($pod->field('ppe_date'));
$address =  $pod->field('ppe_adress');
$vacancy =  $pod->field('ppe_vacancy');
$users =  $pod->field('ppe_users');
$vacancy -= count($users);
//print_r($users, false);
?>


<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> itemscope="" itemtype="http://schema.org/BlogPosting">
<?php
if (!is_category() && !is_archive()){
    echo '<div class="record-content">';
    if ( has_post_thumbnail() ) {
        the_post_thumbnail( 'single-header-thumb', array( 'itemprop' => 'image' ) );
    }
    the_title( '<h1 class="page-header entry-title" itemprop="name headline">','</h1>' );
    echo '</div>';
    ?>

    <!-- Event banner -->
    <div class="pp-event-box">
        <div class="row">
            <div class="col-md-3">
                <div class="pp-event-info">ОСТАЛОСЬ МЕСТ: <b><?php echo $vacancy; ?></b></div>
            </div>
            <div class="col-md-3">
                <div class="pp-event-date">
                    <span class="pp-event-date-big"><?php echo date('d', $event_date); ?></span><span class="pp-event-date-month">/<?php echo date('m', $event_date); ?></span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="pp-event-time">
                    <div class="pp-event-time-week"><?php echo translate_week_name(date('l', $event_date)); ?></div>
                    <div class="pp-event-time-time"><?php echo date('H:i', $event_date); ?></div>
                </div>
            </div>
            <div class="col-md-3">
                <?php the_title('<div class="pp-event-info"><b>', '</b></div>'); ?>
            </div>
        </div>
        <div class="row">
            <div class="pp-event-col4 col-md-12">
                <div class="pp-event-address"><b>АДРЕС:</b> <?php echo $address; ?></div>
            </div>
        </div>
    </div>

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#event-description" aria-controls="event-description" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-info-sign"></span> Описание</a></li>
        <li role="presentation"><a href="#event-map" aria-controls="event-map" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-globe"></span> Карта</a></li>
        <li role="presentation"><a href="#event-comments" aria-controls="event-comments" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-thumbs-up"></span> Комментарии <span class="badge" id="comments_count"><?php echo get_comments_number(); ?></span></a></li>
        <?php if ($event_date>=time() && $vacancy>0) : ?>
        <li role="presentation"><a href="#event-register" aria-controls="event-register" role="tab" data-toggle="tab" class="text-danger"><span class="glyphicon glyphicon-pencil"></span> Регистрация на событие</a></li>
        <?php endif ?>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="event-description">
            <?php the_content('читать далее ...'); ?>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="event-map">
            <?php echo GeoMashup::map('height=500&width=840&zoom=15'); ?>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="event-comments">
            <?php comments_template(); ?>
        </div>
        <?php if ($event_date>=time() && $vacancy>0) : ?>
        <div role="tabpanel" class="tab-pane fade" id="event-register">
            Регистрация
        </div>
        <?php endif ?>
    </div>

    <?php

    get_template_part('templates/block', 'social-share');

} else {
    echo '<div class="pp-event row">';
    echo '<div class="pp-event-col1 col-md-3">';
    if (has_post_thumbnail()) {
        echo '<div class="thumbnail-link"><a href="' . get_permalink() . '" title="' . esc_attr(get_the_title($post_id)) . '">';
        the_post_thumbnail('thumbnail', array('itemprop' => 'image', 'class' => ''));
        echo '</a></div>';
    }
    echo '</div>';
    ?>
    <div class="pp-event-col2 col-md-6">
        <div class="pp-event-date">
            <span class="pp-event-date-big"><?php echo date('d', $event_date); ?></span><span class="pp-event-date-month">/<?php echo date('m', $event_date); ?></span>
        </div>
        <div class="pp-event-time">
            <div class="pp-event-time-week"><?php echo translate_week_name(date('l', $event_date)); ?></div>
            <div class="pp-event-time-time"><?php echo date('H:i', $event_date); ?></div>
        </div>
        <div class="pp-event-address"><b>АДРЕС:</b> <?php echo $address; ?></div>
    </div>
    <div class="pp-event-col3 col-md-3">
        <?php the_title(sprintf('<h4 class="entry-title" itemprop="name headline"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h4>'); ?>
        <div>МЕСТ: <?php echo $vacancy; ?></div>
        <?php
        if ($event_date<time()){
            echo '<a href="' . get_permalink() . '" class="btn btn-info">Подробнее</a>';
        }
        elseif ($vacancy<=0){
            echo '<a href="' . get_permalink() . '" class="btn btn-warning">Регистрация закрыта</a>';
        } else {
            echo '<a href="' . get_permalink() . '#register" class="btn btn-danger">Регистрация</a>';
        }
        ?>

    </div>
    <?php
    echo '</div>';
}
?>
</article><!-- #post-## -->