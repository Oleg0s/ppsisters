<?php
/**
 * User: Oleg0s
 * Date: 07.07.2015
 * Time: 10:38
 */
?>

<div id="secondary" class="widget-area" role="complementary">
    <div class="second-menu">
        <ul class="nav nav-pills nav-stacked">
            <?php get_template_part('templates/menu', 'right'); ?>
        </ul>
    </div>
    <?php dynamic_sidebar( 'sidebar-1' ); ?>
</div><!-- #secondary -->
