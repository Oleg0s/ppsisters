<?php
/**
 * User: Oleg0s
 * Date: 11.07.2015
 * Time: 13:24
 */
?>

<div class="search-box">
    <form method="get" id="searchform" action="<?php echo home_url() ; ?>/">
        <div class="form-group">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Поиск ..." value="<?php echo esc_html($s, 1); ?>" name="s" id="s" maxlength="33">
                <span class="input-group-btn">
                    <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span></button>
                </span>
            </div>
        </div>
    </form>
</div>
