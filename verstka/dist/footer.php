<?php
/**
 * User: Oleg0s
 * Date: 04.07.2015
 * Time: 15:38
 */
?>

<footer>
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 footer-col">
                    <p>Подпишитесь на нас, чтобы быть в курсе событий PP SISTERS:</p>
                </div>
                <div class="col-sm-3 footer-col">
                    <p>Адрес нашей базы:</p>
                </div>
                <div class="col-sm-3 footer-col">
                    <p>Связь с нами:</p>
                </div>
            </div>
        </div>
        <div class="row row-hr"><hr></div>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 footer-col">
                    <form>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div>
                                <input type="text" class="form-control" id="email" placeholder="e-mail">
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-primary">Да</button>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-sm-3 footer-col">
                    <p>Москва,<br>ул. Космонавта Волкова 10</p>
                    <h4 class="phone"><span class="glyphicon glyphicon-earphone"></span>+7 555 555 55 55</h4>
                </div>
                <div class="col-sm-3 footer-col">
                </div>
            </div>
        </div>
    </div>
    <div class="row copyright">
        <div class="col-sm-12">
            © ping pong sisters. 2014-2015. копирование материалов запрещено
        </div>
    </div>
</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/pingpong-theme.js"></script>


</body>
</html>
