<?php
/**
 * User: Oleg0s
 * Date: 04.07.2015
 * Time: 15:35
 */
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Тема PingPongSisiters Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="css/pingpong-theme.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <!-- <link href="css/style.css" rel="stylesheet"> -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body role="document">

<!-- Fixed navbar -->
<nav class="navbar navbar-inverse navbar-fixed-top navbar-expand" id="main-navbar">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><img src="img/logo.png" alt="PingPongSisters"></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Подробнее о нас</a></li>
                <li><a href="#">Магазин</a></li>
                <li><a href="#">Календарь событий</a></li>
                <li><a href="#">Фото-альбом</a></li>
                <li><a href="#">Контакты</a></li>
            </ul>
            <!-- Login -->
            <div class="nav-filler pull-right">
                <div class="nav-user-menu">
                    <a href="#"><span class="hide-on-small">Вход </span><span class="glyphicon glyphicon-log-in" aria-hidden="true"></span></a>
                    &nbsp;&nbsp;|&nbsp;&nbsp;
                    <a href="#"><span class="hide-on-small">Регистрация </span><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                </div>
            </div>
            <!-- Bucket -->
            <div class="nav-filler pull-right">
                <div class="nav-user-menu">
                    <a href="#">
                        <div class="nav-bucket"><img src="img/bucket.png"><span class="badge">42</span></div>
                        <div class="hello nav-bucket-sum hide-on-small">2&nbsp;500р.</div>
                    </a>
                </div>
            </div>
        </div><!--/.nav-collapse -->
    </div>
</nav>
