/**
 * Created by Oleg0s on 02.07.2015.
 */

$(document).ready(function(){
        var contentHeight = $(window).height() - $('#main-navbar').height() - $('footer').height()-5;
        $('.theme-showcase').css('min-height', contentHeight);

        correctMenuHeight();

        $(window).resize(function(){
            correctMenuHeight();
        }).scroll(function() {
            correctMenuHeight();
            if ( $(document).scrollTop() > 50 ) {
                $('#scrollup').fadeIn('slow');
            } else {
                $('#scrollup').fadeOut('slow');
            }
        });

        /*Scroll up image*/
        $('#scrollup').mouseover( function(){
            $( this ).animate({opacity: 0.65},100);
        }).mouseout( function(){
            $( this ).animate({opacity: 0.8},100);
        }).click( function(){
            $('html, body').animate({scrollTop:0}, 1000);
            //window.scroll(0 ,0);
            return false;
        });
    }
);

function correctMenuHeight() {
    if ($(document).scrollTop() > 50 || $(document).width() < 992) {
        $('#main-navbar').addClass('navbar-shrink').removeClass('navbar-expand').css('top', $('#wpadminbar').height());
        $('.hello').fadeOut("slow");
    } else {
        $('#main-navbar').addClass('navbar-expand').removeClass('navbar-shrink').css('top', $('#wpadminbar').height());
        $('.hello').fadeIn("slow");
    }
}
